<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CsvController;
use App\Http\Controllers\DepenseController;
use App\Http\Controllers\FrameworkController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\MyMailController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\TypeActeController;
use App\Http\Controllers\UtilisateurController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Utilisateur

    Route::post('import-csv', [
        CsvController::class,
        'import'
    ])->name('import');

    Route::get('list-depense-utilisateur', [
        UtilisateurController::class,
        'listDepenseUtilisateur'
    ])->name('listDepenseUtilisateur');

    Route::get('create-depense', [
        UtilisateurController::class,
        'createDepense'
    ])->name('createDepense');

    Route::get('add-depense', [
        UtilisateurController::class,
        'addDepense'
    ])->name('addDepense');

    Route::get('generate-facture-patient', [
        UtilisateurController::class,
        'generateFacturePatient'
    ])->name('generateFacturePatient');

    Route::get('list-act-patient', [
        UtilisateurController::class,
        'listActPatient'
    ])->name('list-act-patient');

    Route::get('create-act-patient', [
        UtilisateurController::class,
        'createActPatient'
    ])->name('createActPatient');

    Route::get('add-act-patient', [
        UtilisateurController::class,
        'addActPatient'
    ])->name('addActPatient');

    Route::get('home-utilisateur', [
        UtilisateurController::class,
        'homeUtilisateur'
    ])->name('homeUtilisateur');

    Route::get('check-login-utilisateur', [
        UtilisateurController::class,
        'checkLoginUtilisateur'
    ]);
// 

// RESOURCES

    // Depense
    Route::resource('depenses', DepenseController::class);
    Route::get('depenses/delete-depense/{id}', [
        DepenseController::class,
        'deleteDepense'
    ]);
    Route::post('depenses-update', [
        DepenseController::class,
        'updateDepense'
    ]);
    // 

    // TypeActe
    Route::resource('typeactes', TypeActeController::class);
    Route::get('typeactes/delete-typeacte/{id}', [
        TypeActeController::class,
        'deleteTypeActe'
    ]);
    Route::post('typeactes-update', [
        TypeActeController::class,
        'updateTypeActe'
    ]);
    // 

    // Patient
    Route::resource('patients', PatientController::class);
    Route::get('patients/delete-patient/{id}', [
        PatientController::class,
        'deletePatient'
    ]);
    Route::post('patients-update', [
        PatientController::class,
        'updatePatient'
    ]);
    // 

// 

// Admin

    Route::get('dashboard', [
        AdminController::class,
        'dashboard'
    ]);

    Route::get('check-login-admin', [
        AdminController::class,
        'checkLoginAdmin'
    ]);

    Route::get('login-admin', [
        AdminController::class,
        'loginAdmin'
    ]);

// 

// Reynolds' modif

Route::get('download-pdf', [
    PdfController::class,
    'downloadPdf'
]);

Route::get('generate-pdf', [
    PdfController::class,
    'generatePdf'
]);

Route::get('edit', [
    FrameworkController::class,
    'edit'
]);

Route::get('client-article', [
    FrameworkController::class,
    'getClientArticle'
]);

Route::get('dbName', [
    FrameworkController::class,
    'getDbName'
]);

Route::get('list', [
    FrameworkController::class,
    'list'
]);

// Auto-complete
Route::get('autoComplete', [
    homeController::class,
    'autoComplete'
])->name('autoComplete');

// Email

Route::get('sendEmail', [
    MyMailController::class,
    'sendEmail'
]);

// 

Route::get(
    '/log-out',
    [
        homeController::class,
        'logOut'
    ]
);


Route::get('/createTask', [
    homeController::class,
    'createTask'
]);

Route::post('/createProject', [
    homeController::class,
    'createProject'
]);


// 

Route::get('/', [homeController::class, 'login']);
Route::get('/login', [homeController::class, 'login'])->name('login');
Route::post('/authentify', [homeController::class, 'authentify']);
Route::post('/insert', [homeController::class, 'insert']);
Route::get('/project/{id}', [homeController::class, 'project'])->name('project');
Route::get('/myproject', [homeController::class, 'myproject'])->name('myproject');
