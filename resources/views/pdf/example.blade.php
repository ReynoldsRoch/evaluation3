<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title }}</title>
</head>

<body>

    <center>
        <h1>E-CLINICAL
            @php
                $datetime = Carbon\Carbon::parse($facturePatient->date);
                $date = $datetime->format('Y-m-d');
                $time = $datetime->format('H:i:s');
                $monthLetter = $datetime->format('F');
            @endphp
        </h1>
    </center>

    <p>The : {{ $datetime->day }} {{ $monthLetter }}, {{ $datetime->year }}</p>
    <p>At : {{ $time }}</p>

    @php
        $totalPrix = 0.0;
    @endphp


    <hr>

    <h2>Patient : </h2>
    <p><u>Name : </u> {{ $patient->nom }}</p>
    <p><u>Birthday : </u> {{ $patient->dtn }}</p>
    <p><u>Gender : </u> {{ $patient->genre->sexe }}</p>

    <h2>Acts : </h2>

    <table border="1" style="border-collapse: collapse; width: 100%;">
        <tr>
            <th>Name</th>
            <th>Price</th>
        </tr>

        @foreach ($factureDetails as $factureDetail)
            <tr style="text-align: center">
                <td>{{ $factureDetail->typeacte->nom }}</td>
                <td>{{ $factureDetail->prix }} Ar</td>
            </tr>
            @php
                $totalPrix += $factureDetail->prix;
            @endphp
        @endforeach
        <tr></tr>
        <tr style="text-align: center">
            <td>
                <b>TOTAL</b>
            </td>
            <td>
                <b>{{ $totalPrix }} Ar</b>
            </td>
        </tr>

    </table>

    <h3>User : </h3>
    <p><u>Name : </u> {{ $utilisateur->nom }}</p>
    <p><u>Email : </u> {{ $utilisateur->email }}</p>


</body>

</html>
