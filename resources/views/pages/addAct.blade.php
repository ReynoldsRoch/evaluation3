@extends('pages.layouts.app')

@section('title')
    ADD-ACT-PATIENT
@endsection

@section('content')
    <div class="row">

        <div class="col-lg-8 col-md-8">
            <!-- ======= Contact Section ======= -->
            <section id="contact" class="section-bg">
                <div class="container" data-aos="fade-up">

                    <div class="section-header">
                        <h3>Entering procedures for a patient</h3>
                    </div>


                    <div class="form">
                        <form action="create-act-patient" method="get" class="php-email-form">
                            @csrf
                            <div class="form-group">
                                <select name="idpatient" class="form-control" required>
                                    <option value="" disabled>Choose a patient</option>
                                    @foreach ($patients as $item)
                                        <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>

                            <div id="mere">
                                <div class="form-group" id="act-container">
                                    <div id="act-row">
                                        <select name="type[]" class="form-control" id="act-select">
                                            <option value="" disabled>Choose an act type</option>
                                            @foreach ($actes as $item)
                                                <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><br>

                                <div class="form-group">
                                    <input type="number" value="100000" min="0" required class="form-control"
                                        id="recette" name="price[]" placeholder="Act price">
                                </div><br>

                                <div>
                                    <button type="button" onclick="addActeRow()" class="btn btn-primary btn-floating">
                                        +
                                    </button>
                                </div>
                                <hr>
                            </div>
                            <button type="submit" class="btn btn-success btn-block mb-4">ADD</button>
                        </form>
                    </div>

                </div>
            </section><!-- End Contact Section -->
        </div>
        <div class="col-lg-4 col-md-4">
            <a href="/list-act-patient">
                <button type="button" class="btn btn-primary btn-block mb-4">LIST PATIENT-ACTS</button>
            </a>
        </div>
        
    </div>
    <script>
        function addActeRow() {
            var mere = document.getElementById('mere');
            // console.log(mere);
            var container = document.getElementById('act-container');
            container.classList.add('form-group')
            var rowCount = document.getElementById('act-row').length;

            var newRow = document.createElement('div');
            newRow.classList.add('act-row');

            var selectLabel = document.createElement('br');
            // selectLabel.textContent = 'Types';
            newRow.appendChild(selectLabel);

            var selectElement = document.createElement('select');
            selectElement.name = 'type[]';
            selectElement.classList.add('form-control');

            // Copy options from the first select element
            var firstSelectElement = document.getElementById('act-select');
            console.log(firstSelectElement);
            for (var i = 1; i < firstSelectElement.options.length; i++) {
                var option = document.createElement('option');
                option.value = firstSelectElement.options[i].value;
                option.text = firstSelectElement.options[i].text;
                selectElement.appendChild(option);
            }

            newRow.appendChild(selectElement);

            var recetteInput = document.createElement('input');
            recetteInput.type = 'number';
            recetteInput.value = '50000';
            recetteInput.name = 'price[]';
            recetteInput.classList.add('form-control');
            recetteInput.placeholder = 'Act price';
            mere.appendChild(recetteInput);

            // container.appendChild(newRow);
            mere.appendChild(newRow);

            var hr = document.createElement('hr');
            mere.appendChild(hr);
        }
    </script>
@endsection
