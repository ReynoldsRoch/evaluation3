@extends('pages.layouts.app')

@section('title')
    LIST-USER-DEPENSE
@endsection

@section('content')
    <div class="row">

        <div class="table-responsive">
            <table class="table table-hover align-middle mb-0 bg-white">
                <thead class="bg-light">
                    <tr>
                        <th>DepenseType</th>
                        <th>Price</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($depenseUtilisateurs as $depenseUtilisateur)
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-3">
                                        <p class="fw-bold mb-1">{{ $depenseUtilisateur->typedepense->nom }}</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="fw-normal mb-1">{{ $depenseUtilisateur->prix }} Ar.</p>
                            </td>
                            @php
                                $date = Carbon\Carbon::parse($depenseUtilisateur->date);
                            @endphp
                            <td>
                                <p class="fw-normal mb-1">{{ $date->toDateTimeString() }}</p>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>


    </div>
@endsection
