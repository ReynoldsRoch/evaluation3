@extends('pages.admin.layouts.app')

@section('title')
    CREATE-PATIENT
@endsection

@section('content')
    <div class="row">
        <form action="/patients-update" method="post">
            @csrf
            <!-- Email input -->
            <input type="hidden" name="table" value="patient">
            <input type="hidden" name="id" value="{{ $patient->id }}">
            <div class="form-outline mb-4">
                <input type="text" value="{{ $patient->nom }}" id="loginName" class="form-control" name="nom"
                    required />
                <label class="form-label" for="loginName">Name</label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <input type="date" value="{{ $patient->dtn }}" id="loginPassword" class="form-control" name="dtn"
                    required />
                <label class="form-label" for="loginPassword">Birthday</label>
            </div>

            <div class="form-group">
                <select class="form-control" name="idgenre">
                    @foreach ($genres as $genre)
                        <option value="{{ $genre->id }}" {{ $genre->id == $patient->genre->id ? 'selected' : '' }}>
                            {{ $genre->sexe }}
                        </option>
                    @endforeach
                </select>
            </div><br>

            <h5>Refund : </h5>
            <div class="form-group">
                <select class="form-control" name="remboursement">
                    <option value="0" {{ 0 == $patient->remboursement ? 'selected' : '' }}>
                        False
                    </option>
                    <option value="1" {{ 1 == $patient->remboursement ? 'selected' : '' }}>
                        True
                    </option>
                </select>
            </div><br>

            <!-- Submit button -->
            <button type="submit" class="btn btn-success btn-block mb-4">UPDATE</button>

        </form>
    </div>
@endsection
