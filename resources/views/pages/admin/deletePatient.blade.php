@extends('pages.admin.layouts.app')

@section('title')
    DELETE-PATIENT
@endsection

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h2>CONFIRMATION</h2>
                <h3 class="card-title">{{ $patient->nom }}</h3>
                <p class="card-text"> <u>Birthday :</u> {{ $patient->dtn }}.</p>
                <p class="card-text"> <u>Gender :</u> {{ $patient->genre->sexe }}.</p>
                <p class="card-text"> <u>Refund :</u> {{ $patient->remboursement }}.</p>
                <button type="button" onclick="deleteLieu({{ $patient->id }})"
                    class="btn btn-danger btn-block mb-4">DELETE</button>
            </div>
        </div><br>
        <span id="message" style="visibility: hidden;">
            <div class="alert alert-success" role="alert">
                Deleted successfully!
            </div>
        </span>
    </div>

    <script>
        function deleteLieu(id) {
            console.log(id);
            var xhr = new XMLHttpRequest();
            var url = 'http://127.0.0.1:8000/patients/' + id;

            var csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

            xhr.open('DELETE', url, true);

            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
            xhr.send();

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    console.log(xhr.responseText);
                    document.getElementById('message').style.visibility = 'visible';
                }
            };
        }
    </script>
@endsection
