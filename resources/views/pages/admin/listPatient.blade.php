@extends('pages.admin.layouts.app')

@section('title')
    LIST-PATIENT
@endsection

@section('content')
    <div class="row">

        <div class="col-lg-8 col-md-8">
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th>Name</th>
                            <th>Birthday</th>
                            <th>Gender</th>
                            <th>Refund</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($patients as $patient)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-3">
                                            <p class="fw-bold mb-1">{{ $patient->nom }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="fw-normal mb-1">{{ $patient->dtn }}</p>
                                </td>
                                <td>
                                    <p class="fw-normal mb-1">{{ $patient->genre->sexe }}</p>
                                </td>
                                <td>
                                    @if ($patient->remboursement == 1)
                                        <span class="badge badge-success rounded-pill d-inline">True</span>
                                    @else
                                        <span class="badge badge-danger rounded-pill d-inline">False</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="patients/{{ $patient->id }}/edit">
                                        <span class="badge badge-success rounded-pill d-inline"><i
                                                class="far fa-pen-to-square"></i>EDIT</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="/patients/delete-patient/{{ $patient->id }}">
                                        <span class="badge badge-danger rounded-pill d-inline"><i
                                                class="far fa-trash-can"></i>DELETE</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

        <div class="col-lg-4 col-md-4">
            <a href="/patients/create">
                <button class="btn btn-primary btn-block mb-4">CREATE</button>
            </a>
        </div>



    </div>
@endsection
