@extends('pages.admin.layouts.app')

@section('title')
    EDIT-DEPENSE
@endsection

@section('content')
    <div class="row">
        <center>
            <form action="/depenses-update" method="post">
                @csrf
                <!-- Email input -->
                <input type="hidden" name="table" value="typedepense">
                <input type="hidden" name="id" value="{{ $depense->id }}">
                <div class="form-outline mb-4">
                    <input type="text" value="{{ $depense->nom }}" id="loginName" class="form-control" name="nom"
                        required />
                    <label class="form-label" for="loginName">Name</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="text" value="{{ $depense->code }}" id="loginName" class="form-control" name="code"
                        required />
                    <label class="form-label" for="loginName">Code</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="number" value="{{ $depense->budget }}" id="loginName" class="form-control" name="budget"
                        required />
                    <label class="form-label" for="loginName">Budget</label>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-success btn-block mb-4">UPDATE</button>

            </form>
        </center>
    </div>
@endsection
