@extends('pages.admin.layouts.app')

@section('title')
    DASHBOARD
@endsection

@section('content')
    <style>
        .grid-container {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(2, 1fr);
            /* gap: 5px; */
        }

        .grid-item {
            /* padding: 20px; */
            /* font-size: 30px; */
            text-align: center;
        }
    </style>

    <div class="row">

        <div class="col1 col-lg-2 col-md-0">

            <h2>FILTER</h2>
            <form action="/dashboard" method="get">
                @csrf
                <div class="form-group">
                    <select class="form-control" name="monthFilter">
                        @foreach ($months as $month)
                            <option value="{{ $month->id }}">{{ $month->nom }}
                            </option>
                        @endforeach
                    </select>
                </div><br>
                <div class="form-group">
                    <select class="form-control" name="anneeFilter">
                        @for ($i = 2023; $i >= 1990; $i--)
                            <option value="{{ $i }}">{{ $i }}
                            </option>
                        @endfor
                    </select>
                </div><br>
                <button type="submit" class="btn btn-primary btn-block mb-4">SEARCH</button>
            </form>
            {{-- <form action="dashboard" method="get">
                @csrf
                <div class="form-group">
                    <select class="form-control" name="annee">
                        @for ($i = 2023; $i >= 1990; $i--)
                            <option value="{{ $i }}">{{ $i }}
                            </option>
                        @endfor
                    </select>
                </div><br>
                <button type="submit" class="btn btn-primary btn-block mb-4">SEARCH</button>
            </form> --}}

        </div>

        <div class="col2 col-lg-10 col-md-10">
            <h2>FUNDS ON {{ $annee }}</h2>

            <div class="row">

                <div class="col-lg-6 col-md-6">
                    <canvas id="recetteChart" style="max-width: 500px;"></canvas>
                </div>

                <div class="col-lg-6 col-md-6">
                    <canvas id="depenseChart" style="max-width: 500px;"></canvas>
                </div>

            </div>


        </div>

    </div>
    <hr>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var ctx = document.getElementById("recetteChart").getContext('2d');
        var month = [];

        <?php foreach ($months as $volana) { ?>
        var volana = <?php echo $volana; ?>;
        month.push(volana.nom);
        <?php } ?>

        var prixVente = [];

        <?php foreach ($recettes as $depense) { ?>
        var prix = <?php echo $depense->recettetotal; ?>;
        prixVente[<?php echo $depense->id; ?> - 1] = (prix);
        <?php } ?>

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: month,
                datasets: [{
                    label: 'Recette',
                    // data: [12, 19, 3, 5, 2, 3],
                    data: prixVente,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

    <script>
        var ctx = document.getElementById("depenseChart").getContext('2d');
        var month = [];

        <?php foreach ($months as $volana) { ?>
        var volana = <?php echo $volana; ?>;
        month.push(volana.nom);
        <?php } ?>

        var prixVente = [];

        <?php foreach ($depenses as $depense) { ?>
        var prix = <?php echo $depense->depensetotal; ?>;
        prixVente[<?php echo $depense->id; ?> - 1] = (prix);
        <?php } ?>

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: month,
                datasets: [{
                    label: 'Depense',
                    // data: [12, 19, 3, 5, 2, 3],
                    data: prixVente,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>

    @php
        // recette
        $totalReelRecette = 0;
        $totalBudgetRecette = 0;
        $pourcentageTotalRecette = 0;
        // depense
        $totalReelDepense = 0;
        $totalBudgetDepense = 0;
        $pourcentageTotalDepense = 0;
        // benefice
        $pourcentageBeneficeDepense = 0;
        $resteReelBenefice = 0;
        $resteBudgetBenefice = 0;
        $pourcentageTotalBenefice = 0;
    @endphp

    <div class="row">
        <div class="col-lg-2 col-md-2">
            <h2>Recettes & Benefices</h2>
        </div>

        <div class="col-lg-10 col-md-10">

            <h2>FUNDS ON {{ $mois->nom }}</h2>

            <div class="alert alert-success"><b>Recette</b></div>
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th>TypeActe</th>
                            <th>Real</th>
                            <th>Budget</th>
                            <th>Realisation</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($recetteFilters as $recetteFilter)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-3">
                                            <p class="fw-bold mb-1">{{ $recetteFilter->nom }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="fw-normal mb-1">
                                        {{ number_format((float) $recetteFilter->reel, 2, '.', ',') }} Ar</p>
                                </td>
                                <td>
                                    @php
                                        $budget = number_format((float) $recetteFilter->budget / 12, 2, '.', ',');
                                    @endphp
                                    <p class="fw-normal mb-1">
                                        {{ $budget }}
                                        Ar / month</p>
                                </td>
                                <td>
                                    @php
                                        $realisation = (float) $recetteFilter->reel / ($recetteFilter->budget / 12);
                                        $realisation *= 100.0;
                                        $realisation = number_format((float) $realisation, 2, '.', ',');
                                    @endphp
                                    <p class="fw-normal mb-1">{{ $realisation }} %</p>
                                </td>
                            </tr>
                            @php
                                $totalReelRecette += $recetteFilter->reel;
                                $totalBudgetRecette += $recetteFilter->budget / 12;
                                if ($totalReelRecette != 0 && $totalBudgetRecette != 0) {
                                    # code...
                                    $pourcentageTotalRecette = ($totalReelRecette / $totalBudgetRecette) * 100;
                                }
                            @endphp
                        @endforeach
                        <tr>
                            <td></td>
                            <td>{{ number_format((float) $totalReelRecette, 2, '.', ',') }} Ar</td>
                            <td>{{ number_format((float) $totalBudgetRecette, 2, '.', ',') }} Ar</td>
                            <td>{{ number_format((float) $pourcentageTotalRecette, 2, '.', ',') }} %</td>
                        </tr>

                    </tbody>
                </table>

            </div>

            <hr>

            <div class="alert alert-danger"><b>Depense</b></div>
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th>TypeDepense</th>
                            <th>Real</th>
                            <th>Budget</th>
                            <th>Realisation</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($depenseFilters as $depenseFilter)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-3">
                                            <p class="fw-bold mb-1">{{ $depenseFilter->nom }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="fw-normal mb-1">{{ $depenseFilter->reel }} Ar</p>
                                </td>
                                <td>
                                    @php
                                        $budget = number_format((float) $depenseFilter->budget, 2, '.', ',');
                                    @endphp
                                    <p class="fw-normal mb-1">
                                        {{ $budget }} Ar / month
                                    </p>
                                </td>
                                <td>
                                    @php
                                        $pourcentage = number_format((float) $depenseFilter->realisation, 2, '.', ',');
                                    @endphp
                                    <p class="fw-normal mb-1">
                                        {{ $pourcentage }}
                                        %</p>
                                </td>
                            </tr>
                            @php
                                $totalReelDepense += $depenseFilter->reel;
                                $totalBudgetDepense += $depenseFilter->budget;
                            @endphp
                        @endforeach
                        @php
                            if ($totalReelDepense != 0 && $totalBudgetDepense != 0) {
                                # code...
                                $pourcentageTotalDepense = ($totalReelDepense / $totalBudgetDepense) * 100;
                            }
                        @endphp
                        <tr>
                            <td></td>
                            <td>{{ number_format((float) $totalReelDepense, 2, '.', ',') }} Ar</td>

                            <td>{{ number_format((float) $totalBudgetDepense, 2, '.', ',') }} Ar</td>
                            <td>{{ number_format((float) $pourcentageTotalDepense, 2, '.', ',') }} %</td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <hr>

            <div class="alert alert-primary"><b>Benefice</b></div>
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th></th>
                            <th>Real</th>
                            <th>Budget</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-3">
                                        <p class="fw-bold mb-1">Recette</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="fw-normal mb-1">{{ number_format((float) $totalReelRecette, 2, '.', ',') }} Ar
                                </p>
                            </td>
                            <td>
                                <p class="fw-normal mb-1">{{ number_format((float) $totalBudgetRecette, 2, '.', ',') }} Ar
                                </p>
                            </td>
                            @php
                                $pourcentageBeneficeRecette = 0;
                                if ($totalBudgetRecette != 0) {
                                    # code...
                                    $pourcentageBeneficeRecette = ($totalReelRecette / $totalBudgetRecette) * 100;
                                }
                            @endphp
                            <td>
                                <p class="fw-normal mb-1">
                                    {{ number_format((float) $pourcentageBeneficeRecette, 2, '.', ',') }} %</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div class="ms-3">
                                        <p class="fw-bold mb-1">Depense</p>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <p class="fw-normal mb-1">
                                    {{ number_format((float) $totalReelDepense, 2, '.', ',') }} Ar</p>
                            </td>
                            <td>
                                <p class="fw-normal mb-1">
                                    {{ number_format((float) $totalBudgetDepense, 2, '.', ',') }} Ar</p>

                            </td>
                            @php
                                if ($totalReelDepense != 0 && $totalBudgetDepense != 0) {
                                    # code...
                                    $pourcentageBeneficeDepense = ($totalReelDepense / $totalBudgetDepense) * 100;
                                }
                            @endphp
                            <td>
                                <p class="fw-normal mb-1">
                                    {{ number_format((float) $pourcentageBeneficeDepense, 2, '.', ',') }} %</p>
                            </td>
                        </tr>

                        @php
                            $resteReelBenefice = $totalReelRecette - $totalReelDepense;
                            $resteBudgetBenefice = $totalBudgetRecette - $totalBudgetDepense;
                            if ($resteReelBenefice != 0 && $resteBudgetBenefice != 0) {
                                # code...
                                $pourcentageTotalBenefice = ($resteReelBenefice / $resteBudgetBenefice) * 100;
                            }
                        @endphp
                        <tr>
                            <td></td>
                            <td>{{ number_format((float) $resteReelBenefice, 2, '.', ',') }} Ar</td>

                            <td>{{ number_format((float) $resteBudgetBenefice, 2, '.', ',') }}</td>
                            <td>
                                {{ number_format((float) $pourcentageTotalBenefice, 2, '.', ',') }} %
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
@endsection
