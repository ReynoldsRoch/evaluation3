@extends('pages.admin.layouts.app')

@section('title')
    DELETE-DEPENSE
@endsection

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h2>CONFIRMATION</h2>
                <h3 class="card-title">Name : {{ $depense->nom }}</h3>
                <h4>Code : {{ $depense->code }}</h4>
                <h4>Budget : {{ $depense->budget }} Ar/year</h4>
                <button type="button" onclick="deleteLieu({{ $depense->id }})"
                    class="btn btn-danger btn-block mb-4">DELETE</button>
            </div>
        </div><br>
        <span id="message" style="visibility: hidden;">
            <div class="alert alert-success" role="alert">
                Deleted successfully!
            </div>
        </span>
    </div>

    <script>
        function deleteLieu(id) {
            console.log(id);
            var xhr = new XMLHttpRequest();
            var url = 'http://127.0.0.1:8000/depenses/' + id;

            var csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

            xhr.open('DELETE', url, true);

            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);
            xhr.send();

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    console.log(xhr.responseText);
                    document.getElementById('message').style.visibility = 'visible';
                }
            };
        }
    </script>
@endsection
