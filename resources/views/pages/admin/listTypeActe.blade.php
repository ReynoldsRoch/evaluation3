@extends('pages.admin.layouts.app')

@section('title')
    LIST-ACT-TYPE
@endsection

@section('content')
    <div class="row">

        <div class="col-lg-8 col-md-8">
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Budget</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($typeactes as $typeacte)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-3">
                                            <p class="fw-bold mb-1">{{ $typeacte->nom }}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{ $typeacte->code }}</p>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{ $typeacte->budget }} Ar/year</p>
                                </td>
                                <td>
                                <td>
                                    <a href="typeactes/{{ $typeacte->id }}/edit">
                                        <span class="badge badge-success rounded-pill d-inline"><i
                                                class="far fa-pen-to-square"></i>EDIT</span>
                                    </a>
                                </td>
                                <td>
                                    <a href="/typeactes/delete-typeacte/{{ $typeacte->id }}">
                                        <span class="badge badge-danger rounded-pill d-inline"><i
                                                class="far fa-trash-can"></i>DELETE</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

        <div class="col-lg-4 col-md-4">
            <a href="/typeactes/create">
                <button class="btn btn-primary btn-block mb-4">CREATE</button>
            </a>
        </div>



    </div>
@endsection
