@extends('pages.admin.layouts.app')

@section('title')
    CREATE-ACT-TYPE
@endsection

@section('content')
    <div class="row">
        <center>
            @if (Session::has('error'))
                <div class="alert alert-danger"><b>{{ Session::get('error') }}</b></div>
            @endif
            <form action="/typeactes" method="post">
                @csrf
                <!-- Email input -->
                <input type="hidden" name="table" value="typeacte">
                <div class="form-outline mb-4">
                    <input type="text" value="Circoncision" id="loginName" class="form-control" name="nom" required />
                    <label class="form-label" for="loginName">Name</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="number" value="400000" min="0" id="loginName" class="form-control" name="budget"
                        required />
                    <label class="form-label" for="loginName">Budget</label>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">CREATE</button>

            </form>
        </center>
    </div>
@endsection
