@extends('pages.admin.layouts.app')

@section('title')
    EDIT-ACT-TYPE
@endsection

@section('content')
    <div class="row">
        <center>
            @if (Session::has('error'))
                <div class="alert alert-danger"><b>{{ Session::get('error') }}</b></div>
            @endif
            <form action="/typeactes-update" method="post">
                @csrf
                <!-- Email input -->
                <input type="hidden" name="table" value="typeacte">
                <input type="hidden" name="id" value="{{ $typeacte->id }}">
                <div class="form-outline mb-4">
                    <input type="text" value="{{ $typeacte->nom }}" id="loginName" class="form-control" name="nom"
                        required />
                    <label class="form-label" for="loginName">Name</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="text" value="{{ $typeacte->code }}" id="loginName" class="form-control" name="code"
                        required />
                    <label class="form-label" for="loginName">Code</label>
                </div>
                <div class="form-outline mb-4">
                    <input type="number" value="{{ $typeacte->budget }}" id="loginName" class="form-control" name="budget"
                        required />
                    <label class="form-label" for="loginName">Budget</label>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-success btn-block mb-4">UPDATE</button>

            </form>
        </center>
    </div>
@endsection
