@extends('pages.admin.layouts.app')

@section('title')
    CREATE-PATIENT
@endsection

@section('content')
    <div class="row">
        <center>
            <form action="/patients" method="post">
                @csrf
                <!-- Email input -->
                <input type="hidden" name="table" value="patient">
                <div class="form-outline mb-4">
                    <input type="text" value="LEON SCOTT" id="loginName" class="form-control" name="nom" required />
                    <label class="form-label" for="loginName">Name</label>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                    <input type="date" id="loginPassword" class="form-control" name="dtn" required />
                    <label class="form-label" for="loginPassword">Birthday</label>
                </div>

                <div class="form-group">
                    <select class="form-control" name="idgenre">
                        @foreach ($genres as $genre)
                            <option value="{{ $genre->id }}">{{ $genre->sexe }}
                            </option>
                        @endforeach
                    </select>
                </div><br>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">CREATE</button>

            </form>
        </center>
    </div>
@endsection
