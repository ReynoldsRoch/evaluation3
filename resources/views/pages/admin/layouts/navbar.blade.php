<div class="row">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <!-- Container wrapper -->
        <div class="container-fluid">
            <!-- Navbar brand -->
            <a class="navbar-brand" href="#">ADMIN E-CLINICAL</a>

            <!-- Toggle button -->
            <button class="navbar-toggler" type="button" data-mdb-toggle="collapse"
                data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <i class="fas fa-bars text-light"></i>
            </button>

            <!-- Collapsible wrapper -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left links -->
                <ul class="navbar-nav me-auto d-flex flex-row mt-3 mt-lg-0">
                    <li class="nav-item text-center mx-2 mx-lg-1">
                        <a class="nav-link {{ Request::is('patients') ? 'active' : '' }}" aria-current="page"
                            href="/patients">
                            <div>
                                <i class="fas fa-head-side-mask"></i>
                            </div>
                            Patients
                        </a>
                    </li>
                    <li class="nav-item text-center mx-2 mx-lg-1">
                        <a class="nav-link {{ Request::is('typeactes') ? 'active' : '' }}" aria-current="page"
                            href="/typeactes">
                            <div>
                                <i class="fas fa-capsules"></i>
                            </div>
                            Act
                        </a>
                    </li>
                    <li class="nav-item text-center mx-2 mx-lg-1">
                        <a class="nav-link {{ Request::is('depenses') ? 'active' : '' }}" href="/depenses">
                            <div>
                                <i class="fas fa-coins"></i>
                            </div>
                            Depense
                        </a>
                    </li>
                    <li class="nav-item text-center mx-2 mx-lg-1">
                        <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="/dashboard">
                            <div>
                                <i class="fas fa-pie-chart"></i>
                            </div>
                            Dashboard
                        </a>
                    </li>
                    {{-- <li class="nav-item text-center mx-2 mx-lg-1">
                        <button onclick="window.print()" class="btn btn-primary" type="button" data-mdb-ripple-color="dark">
                            Print this page
                        </button>
                    </li> --}}



                </ul>
                <!-- Left links -->

                <!-- Right links -->
                <ul class="navbar-nav ms-auto d-flex flex-row mt-3 mt-lg-0">
                    <li class="nav-item text-center mx-2 mx-lg-1">
                        <a class="nav-link" href="/login-admin">
                            <div>
                                <i class="fas fa-arrow-right-from-bracket"></i>
                            </div>
                            Disconnect
                        </a>
                    </li>
                </ul>
                <!-- Right links -->

                <!-- Search form -->
                <form class="d-flex input-group w-auto ms-lg-3 my-3 my-lg-0">
                    <input type="search" class="form-control" placeholder="Search" aria-label="Search" />
                    <button class="btn btn-primary" type="button" data-mdb-ripple-color="dark">
                        Search
                    </button>
                </form>
            </div>
            <!-- Collapsible wrapper -->
        </div>
        <!-- Container wrapper -->
    </nav>
    <!-- Navbar -->


</div>
<br />
