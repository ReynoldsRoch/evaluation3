@extends('pages.layouts.app')

@section('title')
    HOME
@endsection

@section('content')
    <style>
        .grid-container {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: repeat(2, 1fr);
            gap: 5px;
        }

        .grid-item {
            /* padding: 20px; */
            /* font-size: 30px; */
            text-align: center;
            margin-bottom: 100px;
        }
    </style>

    <div class="row">

        <div class="col1 col-lg-4 col-md-0">

            <!-- Jumbotron -->
            <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white"
                style="
    background-image: url('https://previews.123rf.com/images/stockgiu/stockgiu1811/stockgiu181103480/127714961-doctor-office-design-concept-of-medical-health-care-hospital-emergency-and-clinic-vector.jpg');
    height: 100%;
    ">
                <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                    <div class="d-flex justify-content-center align-items-center h-100">
                        <span class="text-white mb-0">
                            <h5>E-CLINICAL</h5>
                            <p class="text-white mb-0">
                                Welcome, <br />we are here for you.
                            </p>
                        </span>
                    </div>
                </div>
            </div>
            <!-- Jumbotron -->

        </div>

        <div class="col2 col-lg-8 col-md-12">
            <div class="grid-container">
                @for ($i = 1; $i <= 3; $i++)
                    <div class="grid-item">
                        <div class="card text-center" style="width: 70%;">
                            <div class="card-header">Medicaments</div>
                            <div class="card-body">
                                <h5 class="card-title">Medicament n°{{ $i }}</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.
                                </p>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>

    </div>
@endsection
