@extends('pages.layouts.app')

@section('title')
    LIST-ACT-PATIENT
@endsection

@section('content')
    <div class="row">

        <div class="col">
            <div class="table-responsive">
                <table class="table table-hover align-middle mb-0 bg-white">
                    <thead class="bg-light">
                        <tr>
                            <th>Patient</th>
                            <th>Date</th>
                            <th>Acts</th>
                            <th>Pdf</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($facturePatients as $facturePatient)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ms-3">
                                            <p class="fw-bold mb-1">{{ $facturePatient->patient->nom }}</p>
                                        </div>
                                    </div>
                                </td>
                                @php
                                    $date = Carbon\Carbon::parse($facturePatient->date);
                                @endphp
                                <td>
                                    <p class="fw-normal mb-1">{{ $date->toDateTimeString() }}</p>
                                </td>
                                <td>
                                    <ol>
                                        @foreach ($facturePatient->factureDetails as $factureDetail)
                                            <li>{{ $factureDetail->typeacte->nom }}</li>
                                        @endforeach
                                    </ol>
                                </td>
                                <td>
                                    <a href="generate-facture-patient?id={{ $facturePatient->id }}">
                                        <button type="button" class="btn btn-warning rounded-pill d-inline"><i
                                                class="fas fa-file-pdf"></i> Export</button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

    </div>
@endsection
