@extends('pages.layouts.app')

@section('title')
    ADD-DEPENSE
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <!-- ======= Contact Section ======= -->
            <section id="contact" class="section-bg">
                <div class="container" data-aos="fade-up">

                    <div class="section-header">
                        <h3>Entering depenses</h3>
                        @if (Session::has('monthError'))
                            <div class="alert alert-danger"><b>{{ Session::get('monthError') }}</b></div>
                        @endif
                    </div>

                    <div class="form">
                        <form action="create-depense" method="get">
                            @csrf

                            <div id="mere">
                                <div class="form-group" id="act-container">
                                    <div id="act-row">
                                        <select name="depense" class="form-control" id="act-select">
                                            <option value="" disabled>Choose a depense</option>
                                            @foreach ($depenses as $item)
                                                <option value="{{ $item->id }}">{{ $item->nom }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><br>

                                <div class="form-group">
                                    <label for="">Price : </label>
                                </div>
                                <div class="form-group">
                                    <input type="number" value="100000" min="0" required class="form-control"
                                        id="recette" name="price" placeholder="Depense price">
                                </div><br>

                                <div class="form-group">
                                    <label for="">Day : </label>
                                </div>
                                <div class="form-group">
                                    <input type="number" value="10" min="1" max="31" required
                                        class="form-control" id="date" name="jour" placeholder="Depense day">
                                </div><br>

                                <div class="form-group">
                                    <label for="">Year : </label>
                                </div>
                                <div class="form-group">
                                    <input type="number" value="2023" min="1990" required class="form-control"
                                        id="date" name="year" placeholder="Depense year">
                                </div><br>

                                <!-- Default checkbox -->
                                <div class="form-group">
                                    <label for="">Month : </label>
                                </div>
                                @foreach ($months as $month)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="month-{{ $month->id }}"
                                            value="{{ $month->id }}" id="flexCheckDefault" />
                                        <label class="form-check-label" for="flexCheckDefault">{{ $month->nom }}</label>
                                    </div>
                                @endforeach

                                {{-- <div>
                                    <button type="button" onclick="addActeRow()" class="btn btn-primary btn-floating">
                                        +
                                    </button>
                                </div> --}}
                                <hr>
                            </div>

                            <button type="submit" class="btn btn-success btn-block mb-4">ADD</button>
                        </form>
                    </div>

                </div>
            </section><!-- End Contact Section -->
        </div>

        <div class="col-lg-4 col-md-4">
            <a href="/list-depense-utilisateur">
                <button type="button" class="btn btn-primary btn-block mb-4">LIST DEPENSES</button>
            </a>
            <hr>
            <h3>IMPORT CSV</h3>
            @if (Session::has('codeError'))
                <div class="alert alert-danger"><b>{{ Session::get('codeError') }}</b></div>
            @endif
            <form action="/import-csv" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="file" required class="form-control" name="csv_file" placeholder="CSV_FILE">
                </div><br>
                <button type="submit" class="btn btn-warning btn-block mb-4">IMPORT</button>
            </form>
        </div>

    </div>
@endsection

<script>
    function addActeRow() {
        var mere = document.getElementById('mere');
        // console.log(mere);
        var container = document.getElementById('act-container');
        container.classList.add('form-group')
        var rowCount = document.getElementById('act-row').length;

        var newRow = document.createElement('div');
        newRow.classList.add('act-row');

        var selectLabel = document.createElement('br');
        // selectLabel.textContent = 'Types';
        newRow.appendChild(selectLabel);

        var selectElement = document.createElement('select');
        selectElement.name = 'depense[]';
        selectElement.classList.add('form-control');

        // Copy options from the first select element
        var firstSelectElement = document.getElementById('act-select');
        console.log(firstSelectElement);
        for (var i = 1; i < firstSelectElement.options.length; i++) {
            var option = document.createElement('option');
            option.value = firstSelectElement.options[i].value;
            option.text = firstSelectElement.options[i].text;
            selectElement.appendChild(option);
        }

        newRow.appendChild(selectElement);

        var recetteInput = document.createElement('input');
        recetteInput.type = 'number';
        recetteInput.value = '50000';
        recetteInput.name = 'price[]';
        recetteInput.classList.add('form-control');
        recetteInput.placeholder = 'Depense price';
        mere.appendChild(recetteInput);

        var dateInput = document.createElement('input');
        dateInput.type = 'date';
        dateInput.name = 'date[]';
        dateInput.classList.add('form-control');
        dateInput.placeholder = 'Depense date';
        mere.appendChild(dateInput);

        // container.appendChild(newRow);
        mere.appendChild(newRow);

        var hr = document.createElement('hr');
        mere.appendChild(hr);

    }
</script>
