<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeDepense extends Model
{
    use HasFactory;
    protected $table = 'typedepense';
    public $timestamps = false;

    protected $fillable = [
        'nom'
    ];

    public static function getByCode($code)
    {
        # code...
        $typeDepense = TypeDepense::where('code', $code)->first();
        return $typeDepense;
    }
}
