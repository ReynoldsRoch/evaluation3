<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Admin extends Model
{
    use HasFactory;
    protected $table = 'admin';
    public $timestamps = false;

    public static function log(Request $request)
    {
        # code...
        $admin = Admin::where('email', $request['email'])
            ->where('pwd', md5($request['password']))
            ->first();
        return $admin;
    }
}
