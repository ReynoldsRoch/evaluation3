<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Patient extends Model
{
    use HasFactory;
    protected $table = 'patient';
    public $timestamps = false;

    protected $fillable = [
        'nom',
        'dtn',
        'idgenre',
        'remboursement'
    ];

    public function genre(): BelongsTo
    {
        # code...
        return $this->belongsTo(Genre::class, 'idgenre');
    }
}
