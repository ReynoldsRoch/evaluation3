<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Utilisateur extends Model
{
    use HasFactory;
    protected $table = 'utilisateur';
    public $timestamps = false;

    public static function log(Request $request)
    {
        # code...
        $admin = Utilisateur::where('email', $request['email'])
            ->where('pwd', md5($request['password']))
            ->first();
        return $admin;
    }
}
