<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeActe extends Model
{
    use HasFactory;
    protected $table = 'typeacte';
    public $timestamps = false;

    protected $fillable = [
        'nom'
    ];
}
