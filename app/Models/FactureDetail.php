<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FactureDetail extends Model
{
    use HasFactory;
    protected $table = 'facturedetail';
    public $timestamps = false;

    protected $fillable = [
        'idtypeacte',
        'idfacturepatient',
        'prix'
    ];

    public function typeacte(): BelongsTo
    {
        # code...
        return $this->belongsTo(TypeActe::class, 'idtypeacte');
    }

    public function facturepatient(): BelongsTo
    {
        # code...
        return $this->belongsTo(FacturePatient::class, 'idfacturepatient');
    }
}
