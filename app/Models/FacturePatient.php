<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FacturePatient extends Model
{
    use HasFactory;
    protected $table = 'facturepatient';
    public $timestamps = false;

    protected $fillable = [
        'idpatient'
    ];

    public function patient(): BelongsTo
    {
        # code...
        return $this->belongsTo(Patient::class, 'idpatient');
    }

    public function factureDetails(): HasMany
    {
        # code...
        return $this->hasMany(FactureDetail::class, 'idfacturepatient');
    }
}
