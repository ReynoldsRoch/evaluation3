<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DepenseUtilisateur extends Model
{
    use HasFactory;
    protected $table = 'depenseutilisateur';
    public $timestamps = false;

    public function utilisateur(): BelongsTo
    {
        # code...
        return $this->belongsTo(Utilisateur::class, 'idutilisateur');
    }

    public function typedepense(): BelongsTo
    {
        # code...
        return $this->belongsTo(TypeDepense::class, 'idtypedepense');
    }
}
