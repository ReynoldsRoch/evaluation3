<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Month;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //

    public function dashboard(Request $request)
    {
        # code...
        // $annee = isset($request['annee']) ? $request['annee'] : 2023;
        $annee = isset($request['anneeFilter']) ? $request['anneeFilter'] : 2023;
        // Passe cette annee dans la view recette et depense
        $recetteRequest = 'SELECT
        month.id,
        month.nom as mois,
        COALESCE(SUM(prix), 0) AS recetteTotal
      FROM
        month
        LEFT JOIN (
          SELECT
            factureDetail.prix,
            DATE_PART(\'month\', facturePatient.date) AS month_part
          FROM
            facturePatient
          join factureDetail on factureDetail.idfacturepatient = facturePatient.id
          WHERE
            DATE_PART(\'year\', facturePatient.date) = ' . $annee . '
        ) AS filtered_depenses ON month.id = filtered_depenses.month_part
      GROUP BY
        month.id,
        month.nom
      ORDER BY
        month.id ASC;';

        $depenseRequest = 'SELECT
        month.id,
        month.nom as mois,
        COALESCE(SUM(prix), 0) AS depenseTotal
      FROM
        month
        LEFT JOIN (
          SELECT
            prix,
            DATE_PART(\'month\', date) AS month_part
          FROM
            depenseutilisateur
          WHERE
            DATE_PART(\'year\', date) = ' . $annee . '
        ) AS filtered_depenses ON month.id = filtered_depenses.month_part
      GROUP BY
        month.id,
        month.nom
      ORDER BY
        month.id ASC';

        $data = [
            'annee' => $annee,
            'months' => Month::all(),
            'recettes' => DB::select($recetteRequest),
            'depenses' => DB::select($depenseRequest)
        ];

        // JOUR 2

        $monthFilter = isset($request['monthFilter']) ? $request['monthFilter'] : 7;
        $anneeFilter = isset($request['anneeFilter']) ? $request['anneeFilter'] : 2023;

        // $recetteFilter = 'SELECT v_factureGeneral.nom,Sum(prix) as reel,v_factureGeneral.budget
        // from V_FactureGeneral v_factureGeneral
        // where extract(month from date)=' . $monthFilter . ' and extract(year from date)=' . $anneeFilter . '
        // group by v_factureGeneral.nom,v_factureGeneral.budget';

        $recetteFilter = 'SELECT
        typeacte.id,
        typeacte.nom,
        coalesce(sum(prix), 0) as reel,
        coalesce(v_factureGeneral.budget, typeacte.budget) as budget
    from
        v_factureGeneral
        right join typeacte on v_factureGeneral.idtypeacte = typeacte.id
        and extract(
            month
            from
                v_factureGeneral.date
        ) = ' . $monthFilter . '
        and extract(
            year
            from
                v_factureGeneral.date
        ) = ' . $anneeFilter . '
    where
        typeacte.etat = 0
    group by
        typeacte.id,
        typeacte.nom,
        coalesce(v_factureGeneral.budget, typeacte.budget);';

        // $depenseFilter = 'SELECT V_DepenseGeneral.nomtypedepense,Sum(prixdepenseutilisateur) as reel,V_DepenseGeneral.budgettypedepense
        // from V_DepenseGeneral V_DepenseGeneral
        // where extract(month from datedepenseutilisateur)=' . $monthFilter . ' and extract(year from datedepenseutilisateur)=' . $anneeFilter . '
        // group by V_DepenseGeneral.nomtypedepense,V_DepenseGeneral.budgettypedepense';

        $depenseFilter = 'select
        typedepense.id,
        typedepense.nom,
        coalesce(sum(V_DepenseGeneral.prixdepenseutilisateur), 0) as reel,
        coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget) / 12 as budget,
        (coalesce(sum(V_DepenseGeneral.prixdepenseutilisateur), 0) / (coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget) / 12))*100 as realisation
    from
        V_DepenseGeneral
        right join typedepense on V_DepenseGeneral.idtypedepense = typedepense.id
        and extract(
            month
            from
                V_DepenseGeneral.datedepenseutilisateur
        ) = ' . $monthFilter . '
        and extract(
            year
            from
                V_DepenseGeneral.datedepenseutilisateur
        ) = ' . $anneeFilter . '
    where
        typedepense.etat = 0
    group by
        typedepense.id,
        typedepense.nom,
        coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget);';

        $data['recetteFilters'] = DB::select($recetteFilter);
        $data['depenseFilters'] = DB::select($depenseFilter);
        $data['mois'] = Month::findOrFail($monthFilter);

        // 

        return view('pages.admin.dashboard', $data);
    }

    public function checkLoginAdmin(Request $request)
    {
        # code...
        $admin = Admin::log($request);
        if (!is_null($admin)) {
            return view('pages.admin.home');
        }
        return redirect()->back();
    }

    public function loginAdmin()
    {
        # code...
        return view('pages.admin.login');
    }
}
