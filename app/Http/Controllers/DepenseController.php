<?php

namespace App\Http\Controllers;

use App\Models\Depense;
use App\Models\Month;
use App\Models\TypeDepense;
use Illuminate\Http\Request;

class DepenseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $data = [
            'depenses' => TypeDepense::where('etat', 0)->get()
        ];
        return view('pages.admin.listDepense', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //

        return view('pages.admin.createDepense');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        // $this->myCreate($request);
        $typeActe = new TypeDepense();
        $typeActe->nom = $request['nom'];
        $typeActe->budget = $request['budget'];
        for ($i = 0; $i < 3; $i++) {
            # code...
            $typeActe->code .= $typeActe->nom[$i];
        }
        foreach (TypeDepense::where('etat', 0)->get() as $type) {
            # code...
            if ($type->code == $typeActe->code) {
                # code...
                return redirect()->back()->with('error', 'Code already exists');
            }
        }
        $typeActe->code = strtoupper($typeActe->code);
        $typeActe->save();
        return redirect()->route('depenses.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $data = [
            'depense' => TypeDepense::findOrFail($id)
        ];
        return view('pages.admin.editDepense', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    public function updateDepense(Request $request)
    {
        # code...
        // $this->myUpdate($request);
        $typeActe = TypeDepense::findOrFail($request['id']);
        $typeActe->nom = $request['nom'];
        $typeActe->budget = $request['budget'];
        $typeActe->code = $request['code'];

        $typeActe->update();
        return redirect()->route('depenses.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $sono = TypeDepense::findOrFail($id);
        $sono->etat = 3;
        $sono->update();
    }

    public function deleteDepense(string $id)
    {
        # code...
        $data = [
            'depense' => TypeDepense::findOrFail($id)
        ];
        return view('pages.admin.deleteDepense', $data);
    }

    // CRUD
    function myUpdate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $instance = app()->make($modelName);
        $instance = $instance->find($request['id']);
        foreach ($instance->getFillable() as $fillable) {
            $instance[$fillable] = $request[$fillable];
        }
        $instance->update();
    }
    public function myCreate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $modelInstance = app()->make($modelName);
        foreach ($modelInstance->getFillable() as $fillable) {
            $modelInstance[$fillable] = $request[$fillable];
        }
        $modelInstance->save();
    }
    // 
}
