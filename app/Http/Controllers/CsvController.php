<?php

namespace App\Http\Controllers;

use App\Models\DepenseUtilisateur;
use App\Models\TypeDepense;
use App\Models\Utilisateur;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CsvController extends Controller
{
    //
    public function import(Request $request)
    {
        // Validate the file input
        $request->validate([
            'csv_file' => 'required|mimes:csv,txt'
        ]);

        // Get the uploaded file
        $file = $request->file('csv_file');

        $handle = fopen($file->path(), 'r');

        // Read the file contents
        $contents = file_get_contents($file->path());

        // Split the contents into rows
        $rows = explode(PHP_EOL, $contents);
        // dd($rows);

        // Create an empty array to hold the data
        $data = [];

        // Loop through each row and split it into columns
        // foreach ($rows as $row) {
        //     // $data[] = str_getcsv($row,',');
        // }


        while (($row = fgetcsv($handle, 0, ';')) !== false) {
            $data[] = $row;
        }
        // dd($data);


        fclose($handle);

        $utilisateur = Utilisateur::findOrFail(session()->get('idUtilisateur'));

        for ($i = 0; $i < count($data); $i++) {
            $date = $data[$i][0];
            $code = $data[$i][1];
            $prix = $data[$i][2];

            $typeDepense = TypeDepense::getByCode($code);

            if (is_null($typeDepense)) {
                # code...
                $message = 'Code not found : ' . $code;
                return redirect()->back()->with('codeError', $message);
            } else {
                // echo $date . "<br/>";
                $carbonDate = Carbon::createFromFormat('d/m/Y', $date);
                $formattedDate = $carbonDate->format('Y-m-d');

                // echo "formatted date : " . $formattedDate . "<br/>"; // Output: 2003-03-12
                echo $typeDepense . "<br/>";

                $depenseUtilisateur = new DepenseUtilisateur();
                $depenseUtilisateur->idtypedepense = $typeDepense->id;
                $depenseUtilisateur->idutilisateur = $utilisateur->id;
                $depenseUtilisateur->prix = $prix;
                $depenseUtilisateur->date = $date;
                $depenseUtilisateur->save();

            }
            
            echo "<hr/>";
        }
        return redirect()->route('listDepenseUtilisateur');

        // return view('csv', ['csv' => $data]);
    }
}
