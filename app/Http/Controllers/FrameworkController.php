<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrameworkController extends Controller
{
    //

    public function edit()
    {
        # code...
        return view('pages.edit');
    }

    public function getClientArticle(Request $request)
    {
        # code...
        $article = Client::find($request['idclient'])->articles;
        return response()->json($article);
    }

    public function list()
    {
        # code...
        return view('pages.list');
    }

    public function getDbName()
    {
        # code...
        $dbName = DB::getDatabaseName();
        echo $dbName;
    }
}
