<?php

namespace App\Http\Controllers;

use App\Models\TypeActe;
use Illuminate\Http\Request;

class TypeActeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $data = [
            'typeactes' => TypeActe::where('etat', 0)->get()
        ];
        return view('pages.admin.listTypeActe', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('pages.admin.createTypeActe');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        // $this->myCreate($request);
        $typeActe = new TypeActe();
        $typeActe->nom = $request['nom'];
        $typeActe->budget = $request['budget'];
        for ($i = 0; $i < 3; $i++) {
            # code...
            $typeActe->code .= $typeActe->nom[$i];
        }
        foreach (TypeActe::where('etat', 0)->get() as $type) {
            # code...
            if ($type->code == $typeActe->code) {
                # code...
                return redirect()->back()->with('error', 'Code already exists');
            }
        }
        $typeActe->save();
        return redirect()->route('typeactes.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $data = [
            'typeacte' => TypeActe::findOrFail($id)
        ];
        return view('pages.admin.editTypeActe', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    public function updateTypeActe(Request $request)
    {
        # code...
        // $this->myUpdate($request);
        $typeActe = TypeActe::findOrFail($request['id']);
        $typeActe->nom = $request['nom'];
        $typeActe->budget = $request['budget'];
        $typeActe->code = $request['code'];

        $typeActe->update();
        return redirect()->route('typeactes.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $sono = TypeActe::findOrFail($id);
        $sono->etat = 3;
        $sono->update();
    }

    public function deleteTypeActe(string $id)
    {
        # code...
        $data = [
            'typeActe' => TypeActe::findOrFail($id)
        ];
        return view('pages.admin.deleteTypeActe', $data);
    }

    // CRUD
    function myUpdate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $instance = app()->make($modelName);
        $instance = $instance->find($request['id']);
        foreach ($instance->getFillable() as $fillable) {
            $instance[$fillable] = $request[$fillable];
        }
        $instance->update();
    }
    public function myCreate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $modelInstance = app()->make($modelName);
        foreach ($modelInstance->getFillable() as $fillable) {
            $modelInstance[$fillable] = $request[$fillable];
        }
        $modelInstance->save();
    }
    // 
}
