<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $data = [
            'patients' => Patient::where('etat', 0)->get()
        ];
        return view('pages.admin.listPatient', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $data = [
            'genres' => Genre::all()
        ];
        return view('pages.admin.createPatient', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $this->myCreate($request);
        return redirect()->route('patients.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $data = [
            'patient' => Patient::findOrFail($id),
            'genres' => Genre::all()
        ];
        return view('pages.admin.editPatient', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    public function updatePatient(Request $request)
    {
        # code...
        $this->myUpdate($request);
        return redirect()->route('patients.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $sono = Patient::findOrFail($id);
        $sono->etat = 3;
        $sono->update();
    }

    public function deletePatient(string $id)
    {
        # code...
        $data = [
            'patient' => Patient::findOrFail($id)
        ];
        return view('pages.admin.deletePatient', $data);
    }

    // CRUD
    function myUpdate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $instance = app()->make($modelName);
        $instance = $instance->find($request['id']);
        foreach ($instance->getFillable() as $fillable) {
            $instance[$fillable] = $request[$fillable];
        }
        $instance->update();
    }
    public function myCreate(Request $request)
    {
        $modelName = 'App\Models\\' . $request['table'];
        $modelInstance = app()->make($modelName);
        foreach ($modelInstance->getFillable() as $fillable) {
            $modelInstance[$fillable] = $request[$fillable];
        }
        $modelInstance->save();
    }
    // 
}
