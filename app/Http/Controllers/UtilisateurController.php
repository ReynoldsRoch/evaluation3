<?php

namespace App\Http\Controllers;

use App\Models\DepenseUtilisateur;
use App\Models\FactureDetail;
use App\Models\FacturePatient;
use App\Models\Month;
use App\Models\Patient;
use App\Models\TypeActe;
use App\Models\TypeDepense;
use App\Models\Utilisateur;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use Illuminate\Support\Str;

class UtilisateurController extends Controller
{
    //

    public function createDepense(Request $request)
    {
        # code...
        $utilisateur = Utilisateur::findOrFail(session()->get('idUtilisateur'));

        // $depenses = $request->input('depense');
        // $prices = $request->input('price');
        // $dates = $request->input('date');

        // for ($i = 0; $i < count($depenses); $i++) {
        //     $depenseUtilisateur = new DepenseUtilisateur();
        //     # code...
        //     echo $depenses[$i] . "<br/>";
        //     echo $prices[$i] . "<br/>";
        //     echo $dates[$i] . "<br/>";

        //     $depenseUtilisateur->idtypedepense = $depenses[$i];
        //     $depenseUtilisateur->prix = $prices[$i];
        //     $depenseUtilisateur->date = $dates[$i];
        //     $depenseUtilisateur->idutilisateur = $utilisateur->id;

        //     $depenseUtilisateur->save();
        // }

        $months = Month::all();

        $containsMonth = false;
        $monthError = '';

        foreach ($months as $month) {
            # code...
            if (!is_null($request['month-' . $month->id])) {
                # code...
                $containsMonth = true;

                if ($request['jour'] > 28 && $request['month-' . $month->id] == 2) {
                    # code...
                    $containsMonth = false;
                    $monthError = 'Day > 28 and February is not compatible';
                    break;
                } else {
                    $date = $request['year'] . '-' . $request['month-' . $month->id] . '-' . $request['jour'];
                    // echo $date . "<br/>";

                    $depenseUtilisateur = new DepenseUtilisateur();
                    # code...

                    $depenseUtilisateur->idtypedepense = $request['depense'];
                    $depenseUtilisateur->prix = $request['price'];
                    $depenseUtilisateur->date = $date;
                    $depenseUtilisateur->idutilisateur = $utilisateur->id;

                    $depenseUtilisateur->save();
                }
            }
        }

        if ($containsMonth == false) {
            # code...
            if ($monthError == '') {
                # code...
                $monthError = 'Month expected at least one';
            }
            return redirect()->back()->with('monthError', $monthError);
        }

        return redirect()->route('listDepenseUtilisateur');
    }

    public function listDepenseUtilisateur(Request $request)
    {
        # code...
        $data = [
            'depenseUtilisateurs' => DepenseUtilisateur::where('idutilisateur', session()->get('idUtilisateur'))->get()
        ];
        return view('pages.listDepenseUtilisateur', $data);
    }

    public function addDepense()
    {
        # code...
        $data = [
            'depenses' => TypeDepense::where('etat', 0)->get(),
            'months' => Month::all()
        ];
        return view('pages.addDepense', $data);
    }

    public function generateFacturePatient(Request $request)
    {
        # code...
        $facturePatient = FacturePatient::findOrFail($request['id']);
        $patient = $facturePatient->patient;
        // echo $patient;

        $this->generatePdf($patient, $facturePatient);
    }

    public function listActPatient(Request $request)
    {
        # code...
        $data = [
            'facturePatients' => FacturePatient::where('etat', 0)->get()
        ];
        return view('pages.listActPatient', $data);
    }

    public function createActPatient(Request $request)
    {
        # code...
        $patient = Patient::findOrFail($request['idpatient']);

        $prices = $request->input('price');
        $types = $request->input('type');


        $facturePatient = FacturePatient::create(
            [
                "idpatient" => $patient->id
            ]
        );

        // echo $facturePatient;

        for ($i = 0; $i < count($prices); $i++) {
            # code...
            $price = $prices[$i];
            $typeAct = $types[$i];

            $factureDetail = new FactureDetail();
            $factureDetail->idfacturepatient = $facturePatient->id;
            $factureDetail->idtypeacte = $typeAct;
            $factureDetail->prix = $price;
            $factureDetail->save();
        }

        return redirect()->route('list-act-patient');
    }

    public function generatePdf($patient, $facturePatient)
    {
        # code...
        // Pdf content
        $data = [
            'title' => 'FACTURE-' . $patient->nom,
            'patient' => $patient,
            'utilisateur' => Utilisateur::findOrFail(session()->get('idUtilisateur')),
            'facturePatient' => $facturePatient,
            'factureDetails' => $facturePatient->facturedetails
            // 'clients' => Client::all()
        ];

        $html = view('pdf.example', $data)->render();

        // echo $html;

        $pdf = new Dompdf();

        // Load HTML content
        $pdf->loadHtml($html);

        // // Set the title of the PDF document
        // $pdf->getOptions()->set('title', 'My PDF Document');

        // // Render the PDF document
        $pdf->render();

        // // Get the current directory path
        $directoryPath = public_path('pdfs\\'); // Change the path to your desired folder

        // // Set the output file path and name
        $outputFilePath = $directoryPath . 'facture-' . Str::slug($patient->nom) . '.pdf';

        // echo $outputFilePath . "<br/>";

        // // Save the PDF document to the specified folder
        // $pdf->stream($outputFilePath);

        $pdf->stream('facture-' . Str::slug($patient->nom) . '.pdf');

        echo "PDF saved successfully!";
    }


    public function addActPatient()
    {
        # code...
        $data = [
            'patients' => Patient::where('etat', 0)->get(),
            'actes' => TypeActe::where('etat', 0)->get()
        ];
        return view('pages.addAct', $data);
    }

    public function homeUtilisateur()
    {
        # code...
        return view('pages.home');
    }

    public function checkLoginUtilisateur(Request $request)
    {
        # code...
        $utilisateur = Utilisateur::log($request);
        if (!is_null($utilisateur)) {
            session()->put('idUtilisateur', $utilisateur->id);
            return redirect()->route('homeUtilisateur');
        }
        return redirect()->back();
    }
}
