-- V_DepenseParMois
create
or replace view V_DepenseParMois as
select
  month.id,
  month.nom,
  coalesce(sum(prix), 0) as depenseTotal
from
  depenseutilisateur full
  join month on month.id = DATE_PART('month', date)
  and DATE_PART('year', date) = 2020
group by
  month.id,
  month.nom
order by
  month.id asc;

-- CHAT!!!!!! --
create
or replace view V_DepenseParMois as
SELECT
  month.id,
  month.nom as mois,
  COALESCE(SUM(prix), 0) AS depenseTotal
FROM
  month
  LEFT JOIN (
    SELECT
      prix,
      DATE_PART('month', date) AS month_part
    FROM
      depenseutilisateur
    WHERE
      DATE_PART('year', date) = 2023
  ) AS filtered_depenses ON month.id = filtered_depenses.month_part
GROUP BY
  month.id,
  month.nom
ORDER BY
  month.id ASC;

-- !!!! --
-- V_RecetteParMois
select
  month.id,
  month.nom,
  COALESCE(SUM(prix), 0) AS recetteTotal
from
  facturePatient
  join factureDetail on factureDetail.idfacturepatient = facturePatient.id;

create
or replace view V_RecetteParMois as
SELECT
  month.id,
  month.nom as mois,
  COALESCE(SUM(prix), 0) AS recetteTotal
FROM
  month
  LEFT JOIN (
    SELECT
      factureDetail.prix,
      DATE_PART('month', facturePatient.date) AS month_part
    FROM
      facturePatient
      join factureDetail on factureDetail.idfacturepatient = facturePatient.id
    WHERE
      DATE_PART('year', facturePatient.date) = 2022
  ) AS filtered_depenses ON month.id = filtered_depenses.month_part
GROUP BY
  month.id,
  month.nom
ORDER BY
  month.id ASC;

-- Jour 2

  -- cf script.sql

--