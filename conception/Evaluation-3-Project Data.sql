-- admin
INSERT INTO
  Admin (email, pwd)
VALUES
  ('admin1@example.com', MD5('password123'));

INSERT INTO
  Admin (email, pwd)
VALUES
  ('admin2@example.com', MD5('secret123'));

INSERT INTO
  Admin (email, pwd)
VALUES
  ('admin3@example.com', MD5('secure_password'));

-- typeActe
INSERT INTO
  TypeActe (nom, prix, etat)
VALUES
  ('Consultation', 50000.00, 0),
  ('Operation', 150000.00, 0),
  ('Analyse', 80000.00, 0);

-- typeDepense
INSERT INTO
  TypeDepense (nom)
VALUES
  ('Loyer'),
  ('Salaire');

-- utilisateur
INSERT INTO
  Utilisateur (nom, email, pwd)
VALUES
  (
    'John Doe',
    'john.doe@example.com',
    MD5('password123')
  ),
  (
    'Jane Smith',
    'jane.smith@example.com',
    MD5('securePass456')
  ),
  (
    'Test User',
    'test.user@example.com',
    MD5('test123')
  );

-- genre
INSERT INTO
  Genre (sexe)
VALUES
  ('Male'),
  ('Female');

-- patient
INSERT INTO
  Patient (nom, dtn, idGenre, remboursement, etat)
VALUES
  ('Sarah Lee', '1988-07-25', 2, 0, 0),
  ('Michael Johnson', '1979-03-12', 1, 0, 0),
  ('Emily Chen', '1992-11-05', 2, 0, 0),
  ('William Smith', '1982-09-18', 1, 0, 0);

-- MONTH
INSERT INTO
  Month (nom)
VALUES
  ('January'),
  ('February'),
  ('March'),
  ('April'),
  ('May'),
  ('June'),
  ('July'),
  ('August'),
  ('September'),
  ('October'),
  ('November'),
  ('December');