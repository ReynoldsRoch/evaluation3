-- create database eval3db;
-- \c eval3db;
CREATE TABLE Admin (
  id SERIAL NOT NULL,
  email varchar(255),
  pwd varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE Genre (
  id SERIAL NOT NULL,
  sexe varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE Patient (
  id SERIAL NOT NULL,
  nom varchar(255),
  dtn date DEFAULT now(),
  idGenre int4 NOT NULL,
  remboursement int4 DEFAULT 0,
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE TypeActe (
  id SERIAL NOT NULL,
  nom varchar(255),
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE TypeDepense (
  id SERIAL NOT NULL,
  nom varchar(255),
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE Utilisateur (
  id SERIAL NOT NULL,
  nom varchar(255),
  email varchar(255),
  pwd varchar(255),
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

ALTER TABLE
  Patient
ADD
  CONSTRAINT FKPatient832943 FOREIGN KEY (idGenre) REFERENCES Genre (id);

-- FACTURE
CREATE TABLE FactureDetail (
  id SERIAL NOT NULL,
  idTypeActe int4 NOT NULL,
  prix numeric(10, 2) DEFAULT 0,
  idFacturePatient int4 NOT NULL,
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE FacturePatient (
  id SERIAL NOT NULL,
  idPatient int4 NOT NULL,
  payementClient numeric(10, 2) DEFAULT 0,
  "date" timestamp DEFAULT now(),
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

ALTER TABLE
  FactureDetail
ADD
  CONSTRAINT FKFactureDet495748 FOREIGN KEY (idFacturePatient) REFERENCES FacturePatient (id);

-- DEPENSE
CREATE TABLE DepenseUtilisateur (
  id SERIAL NOT NULL,
  idTypeDepense int4 NOT NULL,
  prix numeric(10, 2),
  "date" date DEFAULT now() NOT NULL,
  idUtilisateur int4 NOT NULL,
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

ALTER TABLE
  DepenseUtilisateur
ADD
  CONSTRAINT FKDepenseUti822343 FOREIGN KEY (idTypeDepense) REFERENCES TypeDepense (id);

ALTER TABLE
  DepenseUtilisateur
ADD
  CONSTRAINT FKDepenseUti297009 FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur (id);

-- MONTH
CREATE TABLE Month (
  id SERIAL NOT NULL,
  nom varchar(255),
  PRIMARY KEY (id)
);

-- J2
ALTER TABLE TypeActe add column code varchar(255) DEFAULT '';
ALTER TABLE TypeActe add column budget numeric(10,2) DEFAULT 0;

ALTER TABLE TypeDepense add column code varchar(255) DEFAULT '';
ALTER TABLE TypeDepense add column budget numeric(10,2) DEFAULT 0;