-- RECETTE
create view V_FactureGeneral as
select
    fd.idfacturepatient,
    fd.idtypeacte,
    ta.nom,
    fd.prix,
    ta.budget,
    f.date
from
    facturedetail fd
    join facturepatient f on f.id = fd.idfacturepatient
    join typeacte ta on ta.id = fd.idtypeacte;

-- drop view detailfacture;
-- recette par mois et annee
-- tafita
select
    v_factureGeneral.nom,
    Sum(prix) as reel,
    v_factureGeneral.budget
from
    V_FactureGeneral v_factureGeneral
where
    extract(
        month
        from
            date
    ) = 7
    and extract(
        year
        from
            date
    ) = 2023
group by
    v_factureGeneral.nom,
    v_factureGeneral.budget;

-- reynolds
select
    typeacte.id,
    typeacte.nom,
    coalesce(sum(prix), 0) as reel,
    coalesce(v_factureGeneral.budget, typeacte.budget) as budget
from
    v_factureGeneral
    right join typeacte on v_factureGeneral.idtypeacte = typeacte.id
    and extract(
        month
        from
            v_factureGeneral.date
    ) = 7
    and extract(
        year
        from
            v_factureGeneral.date
    ) = 2023
where
    typeacte.etat = 0
group by
    typeacte.id,
    typeacte.nom,
    coalesce(v_factureGeneral.budget, typeacte.budget);

-- 
-- DEPENSE
create view V_DepenseGeneral as
select
    depenseutilisateur.id as iddepenseutilisateur,
    depenseutilisateur.prix as prixdepenseutilisateur,
    depenseutilisateur.date as datedepenseutilisateur,
    utilisateur.id as idutilisateur,
    utilisateur.nom as nomutilisateur,
    utilisateur.email as emailutilisateur,
    typedepense.id as idtypedepense,
    typedepense.nom as nomtypedepense,
    typedepense.code as codetypedepense,
    typedepense.budget as budgettypedepense
from
    depenseutilisateur
    join utilisateur on utilisateur.id = depenseutilisateur.idutilisateur
    join typedepense on typedepense.id = depenseutilisateur.idtypedepense;

select
    V_DepenseGeneral.nomtypedepense,
    Sum(prixdepenseutilisateur) as reel,
    V_DepenseGeneral.budgettypedepense
from
    V_DepenseGeneral V_DepenseGeneral
where
    extract(
        month
        from
            datedepenseutilisateur
    ) = 7
    and extract(
        year
        from
            datedepenseutilisateur
    ) = 2023
group by
    V_DepenseGeneral.nomtypedepense,
    V_DepenseGeneral.budgettypedepense;

-- reynolds v2
select
    typedepense.id,
    typedepense.nom,
    coalesce(sum(V_DepenseGeneral.prixdepenseutilisateur), 0) as reel,
    coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget) / 12 as budget,
    (coalesce(sum(V_DepenseGeneral.prixdepenseutilisateur), 0) / (coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget) / 12))*100 as realisation
from
    V_DepenseGeneral
    right join typedepense on V_DepenseGeneral.idtypedepense = typedepense.id
    and extract(
        month
        from
            V_DepenseGeneral.datedepenseutilisateur
    ) = 7
    and extract(
        year
        from
            V_DepenseGeneral.datedepenseutilisateur
    ) = 2023
where
    typedepense.etat = 0
group by
    typedepense.id,
    typedepense.nom,
    coalesce(V_DepenseGeneral.budgettypedepense, typedepense.budget);

--