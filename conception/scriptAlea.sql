-- Clear datas
truncate facturepatient restart identity cascade;

truncate depenseutilisateur restart identity cascade;

truncate patient restart identity;

truncate typeacte restart identity;

truncate typedepense restart identity cascade;
